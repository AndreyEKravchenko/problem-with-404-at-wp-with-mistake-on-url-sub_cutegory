<?php

get_header();

$qo = get_queried_object(); 
if (!empty($qo->term_id)) {
    $cat_id = $qo->term_id;   
}

$term = get_term_by('slug', 'divany', 'product_cat');
$category_id = $term->term_id;

$url = ((!empty($_SERVER['HTTPS'])) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$path = parse_url($url, PHP_URL_PATH);
$parts = explode('/', $path);
$sub_category = $parts[2];

$term = get_term_by('slug', $sub_category, 'product_cat');
$category_id = $term->term_id;
if(empty($category_id)){
    wp_redirect( 'wp-content/themes/furman/404.php' );
    exit;
}
